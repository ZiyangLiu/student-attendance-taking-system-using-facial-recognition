from deepface import DeepFace

model_name = "VGG-Face"
model = DeepFace.build_model(model_name)

# Save the model
model.save('deepface_model.h5')
